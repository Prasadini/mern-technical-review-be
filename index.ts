import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import * as Mongoose from "mongoose";
import { routes } from './routes';
import cors from 'cors';


dotenv.config();

const app: Express = express();
const port = process.env.PORT;
const db_url = process.env.MONGO_CONNECTION_STRING;
const allowedOrigins = ['http://localhost:3000'];

const options: cors.CorsOptions = {
  origin: allowedOrigins
};

app.use(cors(options));
app.use(express.json());
//import routes
app.use('/', routes);

//database
Mongoose.connect(db_url as string).then(()=>{
  console.log("DB connected.")
})
.catch((err)=>{
  console.log("db connection error",err);
})

app.get('/', (req: Request, res: Response) => {
  res.send('Express + TypeScript Server');
});

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});