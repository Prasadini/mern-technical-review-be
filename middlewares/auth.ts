import * as jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from "express"

function verifyAuthUser(req :Request,res:Response,next:NextFunction){
    const token = req.headers["authorization"]?.split(' ')[1];
    console.log(req.headers,token);

    if(token){
        jwt.verify(token as string,process.env.JWT_SCRETE_KEY as string,(err,decoded)=>{
            if(err){
                return res.json({
                    isLoogedIn:false,
                    message:"Unauthorized."+err.message
                })
            }
            next();

        })
    }else{
        res.json({
            message:"Invalid token.",
            isLoogedIn:false
        })
    }
}

export {verifyAuthUser};