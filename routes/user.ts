import { Express, Router } from "express";
import { UserModel } from "../models/users.model";
import * as bcrypt from "bcrypt";
import { verifyAuthUser } from "../middlewares/auth";

export const userRoute = Router();

userRoute.post('/user/create',async (req,res)=>{
    const userDetails = req.body;

    //check user already exists
    const existUserEmail = await UserModel.findOne({email:userDetails.email});
    const existPhoneNumber = await UserModel.findOne({phoneNumber:userDetails.phoneNumber});
    
    if(existUserEmail){
        return res.status(500).json({
            message:"User already Exists with this email address."
        });
    }
    if(existPhoneNumber){
        return res.status(500).json({
            message:"User already Exists with this phone number."
        });
    }
    let newUser = new UserModel(req.body);
    newUser.password = await bcrypt.hash(req.body.password,10);
    newUser.save((err)=>{
        if(err){
            return res.status(400).json({
                error:err.message
            });
        }
        return res.status(200).json({
            message:"User saved successfully."
        });
    });
})

//express http req
userRoute.get('/user',verifyAuthUser,(req,res)=>{
    //mongodb query function
    UserModel.find().exec((err,data)=>{
        if(err){
            return res.status(400).json({
                error:err.message
            });
        }
        return res.status(200).json({
            data:data
        });
    });
})


userRoute.put('/user/edit/:id',verifyAuthUser,(req,res)=>{

    UserModel.findByIdAndUpdate(
        req.params.id,
        {
            $set:req.body
        },
        (err:any, user:any)=>{
            if(err){
                return res.status(400).json({
                    error:err.message
                });
            }
            return res.status(200).json({
                message:"User saved successfully.",
                user: user
            }); 
            
        })
    
})

userRoute.delete('/user/delete/:id',verifyAuthUser,(req,res)=>{
    UserModel.findByIdAndDelete(req.params.id).exec((err:any,user:any)=>{
        if(err){
            return res.status(400).json({
                error:err.message
            });
        }
        return res.status(200).json({
            message:"User deleted successfully.",
            user: user
        }); 
    })
})