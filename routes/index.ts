import { Express, Router } from "express";
import { LoginRoute } from "./login";
import { ResetPassword } from "./resetPassword";
import { userRoute } from "./user";

export const routes = Router();

routes.use(userRoute);
routes.use(LoginRoute)
routes.use(ResetPassword)