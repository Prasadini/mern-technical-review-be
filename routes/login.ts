import { Express, Router } from "express";
import { UserModel } from "../models/users.model";
import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";

export const LoginRoute = Router();

LoginRoute.post('/login',async (req,res)=>{
    const loginDetails = req.body;

    UserModel.findOne({email:loginDetails.email})
    .then(user =>{
        if(!user){
            return res.status(500).json({
                message:"Invalid email or password"
            });
        }
        bcrypt.compare(loginDetails.password, user.password)
        .then(isValidPassword =>{
            if(isValidPassword){
                const jwtPaylod = {
                    id:user.id,
                    email:user.email
                }
                jwt.sign(
                    jwtPaylod,
                    process.env.JWT_SCRETE_KEY as string,
                    {expiresIn:86400},
                    (err, token)=>{
                        console.log(err+"dddd");
                        if(err != null){
                            return res.status(400).json({
                                error:err.message
                            });
                        }
                        return res.status(200).json({
                            message:"logged successfully.",
                            token:token,
                            user:user
                        });
                    }

                )
            }else{
                return res.status(500).json({
                    message:"Invalid email or password"
                });
            }
        })
    });
})