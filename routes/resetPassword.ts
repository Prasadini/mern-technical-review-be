import { Express, Router } from "express";
import { UserModel } from "../models/users.model";
import * as bcrypt from "bcrypt";
import { verifyAuthUser } from "../middlewares/auth";


export const ResetPassword = Router();

ResetPassword.post('/reset-password',verifyAuthUser, async (req,res)=>{
    const passwordDetails = req.body;
    const user = await UserModel.findOne({email:passwordDetails.email});

    if(!user){
        return res.status(500).json({
            message:"Email address not found."
        });
    }else{
        const newPassword = await bcrypt.hash(req.body.password,10);
        UserModel.findByIdAndUpdate(
            user.id,
            {
                $set:{ 'password': newPassword} 
            },
            (err:any, user:any)=>{
                if(err){
                    console.log(err)
                    return res.status(400).json({
                        error:err.message
                    });
                }
                return res.status(200).json({
                    message:"Password reset successfully.",
                    user: user
                }); 
                
            })
    }
   
})